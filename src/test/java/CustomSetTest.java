import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class CustomSetTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldReturnEmptyArrayIfNoElementsAreInserted() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        assertNotNull(customSet.size());
        assertEquals(0, customSet.size().intValue());
        assertArrayEquals(new Integer[]{}, customSet.toArray());
    }

    @Test
    public void shouldThrowExceptionIfNullIsAddedAsAnElement() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("The Element is null");
        customSet.add(null);
    }

    @Test
    public void shouldBeAbleToAdd1ToTheSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        assertEquals(1, customSet.size().intValue());
        assertArrayEquals(new Integer[]{1}, customSet.toArray());
    }

    @Test
    public void shouldBeAbleToAdd2ToTheSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(2);
        assertEquals(1, customSet.size().intValue());
        assertArrayEquals(new Integer[]{2}, customSet.toArray());
    }

    @Test
    public void shouldBeAbleToMultipleElementsToTheSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(2);
        customSet.add(100);
        customSet.add(102);
        assertEquals(3, customSet.size().intValue());
        assertArrayEquals(new Integer[]{2, 100, 102}, customSet.toArray());
    }

    @Test
    public void shouldBeAbleToAddMultipleElementsOfStringTypeToSet() throws Exception {
        CustomSet<String> customSet = new CustomSet<>();
        customSet.add("HELLO");
        customSet.add("WORLD");
        assertEquals(2, customSet.size().intValue());
        assertArrayEquals(new String[]{"HELLO", "WORLD"}, customSet.toArray());
    }

    @Test
    public void shouldBeAbleToAddMultipleElementsOfCustomTypeToSet() throws Exception {
        class NewType {

        }
        CustomSet<NewType> customSet = new CustomSet<>();
        NewType newElement = new NewType();
        NewType anotherElement = new NewType();
        customSet.add(newElement);
        customSet.add(anotherElement);
        assertEquals(2, customSet.size().intValue());
        assertArrayEquals(new NewType[]{newElement, anotherElement}, customSet.toArray());
    }

    @Test
    public void shouldNotAddElementsWhichAreAlreadyAdded() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        customSet.add(1);
        customSet.add(1);
        customSet.add(2);
        customSet.add(2);
        customSet.add(2);
        customSet.add(3);
        customSet.add(3);
        customSet.add(3);
        assertEquals(3, customSet.size().intValue());
        assertArrayEquals(new Integer[]{1, 2, 3}, customSet.toArray());
    }

    @Test
    public void shouldThrowNullPointerExceptionIfNullIsRemoved() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("The Element is null");
        customSet.remove(null);
    }

    @Test
    public void shouldReturnFalseIfSetIsEmptyOnRemovalOfElement() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        assertFalse(customSet.remove(1));
    }

    @Test
    public void shouldReturnFalseIfElementNotFoundInSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        customSet.add(2);
        assertFalse(customSet.remove(3));
    }

    @Test
    public void shouldRemoveElementFromSetAndReturnTrueIfElementFoundInSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        customSet.add(2);
        assertTrue(customSet.remove(1));
        assertEquals(1, customSet.size().intValue());
        assertArrayEquals(new Integer[]{2}, customSet.toArray());
    }

    @Test
    public void shouldRemoveElementFromSetAndReturnTrueIfElementFoundInSetLastIndex() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        customSet.add(2);
        assertTrue(customSet.remove(2));
        assertEquals(1, customSet.size().intValue());
        assertArrayEquals(new Integer[]{1}, customSet.toArray());
    }

    @Test
    public void shouldReturnEmptySetIfAllElementsAreRemoved() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        customSet.add(2);
        customSet.add(3);
        customSet.add(4);
        assertEquals(4, customSet.size().intValue());
        customSet.removeAll();
        assertEquals(0, customSet.size().intValue());
        assertArrayEquals(new Integer[]{}, customSet.toArray());
    }

    @Test
    public void shouldThrowExceptionIfNullIsAddedToAddAll() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        expectedException.expect(NullPointerException.class);
        customSet.addAll(null);
    }

    @Test
    public void shouldAddEmptyArgsToSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        customSet.add(2);
        customSet.add(3);
        customSet.add(4);
        customSet.addAll();
        assertEquals(4, customSet.size().intValue());
        assertArrayEquals(new Integer[]{1, 2, 3, 4}, customSet.toArray());
    }

    @Test
    public void shouldAddMultipleElements() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        customSet.add(2);
        customSet.add(3);
        customSet.add(4);
        customSet.addAll(5, 6, 7, 8, 9);
        assertEquals(9, customSet.size().intValue());
        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, customSet.toArray());
    }

    @Test
    public void shouldThrowNullPointerExceptionOnNullAsElementContains() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        expectedException.expect(NullPointerException.class);
        customSet.contains(null);
    }

    @Test
    public void shouldReturnFalseIfElementNotInSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        assertFalse(customSet.contains(1));
    }

    @Test
    public void shouldReturnTrueIfElementInSet() throws Exception {
        CustomSet<Integer> customSet = new CustomSet<>();
        customSet.add(1);
        assertTrue(customSet.contains(1));
    }

}
