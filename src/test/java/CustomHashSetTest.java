import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class CustomHashSetTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldReturnEmptyArrayIfNoElementsAreInserted() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        assertNotNull(customHashSet.size());
        assertEquals(0, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{}, customHashSet.toArray());
    }

    @Test
    public void shouldThrowExceptionIfNullIsAddedAsAnElement() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        expectedException.expect(NullPointerException.class);
        customHashSet.add(null);
    }

    @Test
    public void shouldBeAbleToAdd1ToTheSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        assertEquals(1, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{1}, customHashSet.toArray());
    }

    @Test
    public void shouldBeAbleToAdd2ToTheSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(2);
        assertEquals(1, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{2}, customHashSet.toArray());
    }

    @Test
    public void shouldBeAbleToMultipleElementsToTheSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(2);
        customHashSet.add(100);
        customHashSet.add(102);
        assertEquals(3, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{2, 100, 102}, customHashSet.toArray());
    }

    @Test
    public void shouldBeAbleToAddMultipleElementsOfStringTypeToSet() throws Exception {
        CustomHashSet<String> customHashSet = new CustomHashSet<>();
        customHashSet.add("HELLO");
        customHashSet.add("WORLD");
        assertEquals(2, customHashSet.size().intValue());
        assertArrayEquals(new String[]{"HELLO", "WORLD"}, customHashSet.toArray());
    }

    @Test
    public void shouldBeAbleToAddMultipleElementsOfCustomTypeToSet() throws Exception {
        class NewType {

        }
        CustomHashSet<NewType> customHashSet = new CustomHashSet<>();
        NewType newElement = new NewType();
        NewType anotherElement = new NewType();
        customHashSet.add(newElement);
        customHashSet.add(anotherElement);
        assertEquals(2, customHashSet.size().intValue());
        assertArrayEquals(new NewType[]{newElement, anotherElement}, customHashSet.toArray());
    }

    @Test
    public void shouldntAddElementsWhichAreAlreadyAdded() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        customHashSet.add(1);
        customHashSet.add(1);
        customHashSet.add(2);
        customHashSet.add(2);
        customHashSet.add(2);
        customHashSet.add(3);
        customHashSet.add(3);
        customHashSet.add(3);
        assertEquals(3, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{1, 2, 3}, customHashSet.toArray());
    }

    @Test
    public void shouldThrowNullPointerExceptionIfNullIsRemoved() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        expectedException.expect(NullPointerException.class);
        customHashSet.remove(null);
    }

    @Test
    public void shouldReturnFalseIfSetIsEmptyOnRemovalOfElement() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        assertFalse(customHashSet.remove(1));
    }

    @Test
    public void shouldReturnFalseIfElementNotFoundInSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        customHashSet.add(2);
        assertFalse(customHashSet.remove(3));
    }

    @Test
    public void shouldRemoveElementFromSetAndReturnTrueIfElementFoundInSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        customHashSet.add(2);
        assertTrue(customHashSet.remove(1));
        assertEquals(1, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{2}, customHashSet.toArray());
    }

    @Test
    public void shouldRemoveElementFromSetAndReturnTrueIfElementFoundInSetLastIndex() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        customHashSet.add(2);
        assertTrue(customHashSet.remove(2));
        assertEquals(1, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{1}, customHashSet.toArray());
    }

    @Test
    public void shouldReturnEmptySetIfAllElementsAreRemoved() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        customHashSet.add(2);
        customHashSet.add(3);
        customHashSet.add(4);
        assertEquals(4, customHashSet.size().intValue());
        customHashSet.removeAll();
        assertEquals(0, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{}, customHashSet.toArray());
    }

    @Test
    public void shouldThrowExceptionIfNullIsAddedToAddAll() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        expectedException.expect(NullPointerException.class);
        customHashSet.addAll(null);
    }

    @Test
    public void shouldAddEmptyArgsToSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        customHashSet.add(2);
        customHashSet.add(3);
        customHashSet.add(4);
        customHashSet.addAll();
        assertEquals(4, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{1, 2, 3, 4}, customHashSet.toArray());
    }

    @Test
    public void shouldAddMultipleElements() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        customHashSet.add(2);
        customHashSet.add(3);
        customHashSet.add(4);
        customHashSet.addAll(5, 6, 7, 8, 9);
        assertEquals(9, customHashSet.size().intValue());
        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, customHashSet.toArray());
    }

    @Test
    public void shouldThrowNullPointerExceptionOnNullAsElementContains() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        expectedException.expect(NullPointerException.class);
        customHashSet.contains(null);
    }

    @Test
    public void shouldReturnFalseIfElementNotInSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        assertFalse(customHashSet.contains(1));
    }

    @Test
    public void shouldReturnTrueIfElementInSet() throws Exception {
        CustomHashSet<Integer> customHashSet = new CustomHashSet<>();
        customHashSet.add(1);
        assertTrue(customHashSet.contains(1));
    }
}
