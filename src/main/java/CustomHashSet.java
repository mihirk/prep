import java.util.LinkedHashMap;
import java.util.Map;

public class CustomHashSet<E> {

    private Map<E, E> map;

    public CustomHashSet() {
        map = new LinkedHashMap<>();
    }

    public Integer size() {
        return map.size();
    }

    public E[] toArray() {
        return (E[]) map.keySet().toArray();
    }

    public void add(E element) {
        nullCheck(element);
        map.put(element, element);
    }

    private void nullCheck(Object element) {
        if (element == null) throw new NullPointerException();
    }

    public Boolean remove(E element) {
        nullCheck(element);
        return map.remove(element) != null;
    }

    public void removeAll() {
        map = new LinkedHashMap<>();
    }

    @SafeVarargs
    public final void addAll(E... elements) {
        nullCheck(elements);
        for (E element : elements) add(element);
    }

    public Boolean contains(E element) {
        nullCheck(element);
        return map.containsKey(element);
    }
}
