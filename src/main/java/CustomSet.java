import java.util.Arrays;

public class CustomSet<E> {
    private E[] elements;

    public CustomSet() {
        elements = (E[]) new Object[0];
    }

    public Boolean contains(E element) {
        nullCheck(element);
        return Arrays.asList(elements).contains(element);
    }

    private Boolean isEmpty() {
        return size() == 0;
    }

    private int indexOf(E element) {
        return Arrays.asList(elements).indexOf(element);
    }


    private void nullCheck(E elementToRemove) {
        if (elementToRemove == null) throw new NullPointerException("The Element is null");
    }

    public Integer size() {
        return elements.length;
    }

    public E[] toArray() {
        return elements;
    }

    public void add(E newElement) {
        nullCheck(newElement);
        if (contains(newElement)) return;
        Integer currentSize = size();
        elements = Arrays.copyOf(elements, ++currentSize);
        elements[--currentSize] = newElement;
    }


    public Boolean remove(E elementToRemove) {
        nullCheck(elementToRemove);
        if (isEmpty() || !contains(elementToRemove)) return false;
        Integer currentSize = size();
        int elementIndex = indexOf(elementToRemove);
        E[] newSet = Arrays.copyOf(elements, --currentSize);
        if (elementIndex < currentSize) newSet[elementIndex] = elements[currentSize];
        elements = newSet;
        return true;
    }

    public void removeAll() {
        elements = Arrays.copyOf(elements, 0);
    }


    @SafeVarargs
    public final void addAll(E... newElements) {
        for (E newElement : newElements) add(newElement);
    }

}
